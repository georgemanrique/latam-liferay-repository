'use strict';

var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var liferayThemeTasks = require('liferay-theme-tasks');

var jsFiles = 'src/js/**/*.js';
var destJsFiles = 'build/js/';

liferayThemeTasks.registerTasks({
	gulp: gulp,
	sassOptions: {outputStyle: 'compressed'},
    hookFn: function (gulp) {
        gulp.hook('before:build:war', function (done) {
            // Fires before build:src task
            console.log("BEFORE build:war > Just printing something to demonstrate this is working");

            // IMPORTANT!!
            // This hook MUST return a valid Gulp Stream and call the done function at the end
            return gulp.src(jsFiles)
                .pipe(concat('all-cemex-scripts.js'))
                .pipe(gulp.dest(destJsFiles))
                .pipe(rename('all-cemex-scripts.min.js'))
                .pipe(uglify())
                .pipe(gulp.dest(destJsFiles))
                .on('end', done);
		});
	}		
});