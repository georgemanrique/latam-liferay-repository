/*!
 * stock.js
 * Created by Santiago Prada
 * Copyright © 2016 IBM. All rights reserved.
 */

var ExchangeModel = function() {
    this.stockValue = 0;
    this.change = 0;
    this.currency = "";
}


var Stock = function(parentView, applicationProperties) {

    var that = this;

    this.applicationProperties = applicationProperties

    //Load component CSS
    var cssId = 'stock-css';
    if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.href = 'components/stock/stock.css';

        //Insert CSS before Main Css
        var mainCSS = document.getElementById(that.applicationProperties.mainCSSId);
        if (mainCSS) {
            head.insertBefore(link, mainCSS);
        } else {
            head.append(link);
        }
    }

    this.init = function() {

        var activeExchange = "NYSWdata";

        //Properties
        this.stockValueContainer = $("#stock-value", parentView);
        this.stockValueLabel = $("#stock-value-text", parentView);
        this.stockValueCurrency = $("#stock-value-status-currency", parentView);
        this.stockValueStatus = $("#stock-value-status", parentView);
        this.marketOptionNYSE = $("#market-option-nyse", parentView);
        this.marketOptionMEX = $("#market-option-mex", parentView);
        this.stockValueStatusCurrency = $("#stock-value-status-currency", parentView);

        //Exchange Models
        //NYSE
        this.NYSWdata = new ExchangeModel();
        this.NYSWdata.currency = "USD";
        //MEX
        this.MEXdata = new ExchangeModel();
        this.MEXdata.currency = "MXN"

        this.updateUI = function() {

            this.stockValueContainer.hide();
            //Update Stock Price

            var value = that[activeExchange]["stockValue"];
            var valueArray = String(value).split(".");
            var decimals = (valueArray[1])? valueArray[1] : "0.0"
            var valueString = valueArray[0] +".<sup>"+decimals+"</sup>"

            this.stockValueLabel.html(valueString);
            //Update Arrow based on value change
            if (Number(that[activeExchange]["change"]) < 0) {
                $("img", that.stockValueStatus).attr("src", "images/stockDownArrowWhite.svg");
            } else {
                $("img", that.stockValueStatus).attr("src", "images/stockUpArrowWhite.svg");
            };

            //Update buttons
            that.marketOptionNYSE.removeClass("active");
            that.marketOptionMEX.removeClass("active");

            if (activeExchange == "NYSWdata") {
                this.marketOptionNYSE.addClass("active");
            } else {
                that.marketOptionMEX.addClass("active");
            }
            //Update currency and buttons
            $(this.stockValueStatusCurrency).html(this[activeExchange]["currency"]);
            this.stockValueContainer.fadeIn("100");
        };


        // Switch between stock exanges
        this.marketOptionNYSE.click(function() {
            activeExchange = "NYSWdata";
            that.updateUI();
        });

        this.marketOptionMEX.click(function() {
            activeExchange = "MEXdata";
            that.updateUI();
        });

        //Load Stock Data

        // IMPORTANT!!!! -> DATA API IS FOR DEVELOPMENT PORPUSES ONLY, GOOGE API HAVE BEEN DEPRECATED
        this.loadNYSEdata = function() {
            $.ajax({
                url: "https://finance.google.com/finance/info?client=ig&q=NSE:,CX",
                dataType: "jsonp",
                success: function(response) {
                    that.NYSWdata["stockValue"] = response[0]["l"];
                    that.NYSWdata["change"] = response[0]["c"];
                    that.updateUI();
                }
            })
        };

        this.loadMEXdata = function() {
            //Data not available for development, fake data in place until integration with web services
            that.MEXdata["stockValue"] = "16.<sup>84</sup>";
            that.MEXdata["change"] = "0";
            that.updateUI();
        };
        this.loadNYSEdata();
        this.loadMEXdata();

    };


    //Load Template
    // PRODUCTION - Load html template from server
    // var htmlTemplate = $(parentView).load("components/stock/stockTemplate.html", function() {
    //     that.init();
    // });

    // DEVELOPMENT - Local Template
    var htmlTemplate = '<div id="stock-component">\
        <div id="stock-market">\
            <span id="market-option-nyse" class="market-option active">NYSE</span><span class="divider"></span><span id="market-option-mex" class="market-option">BMV</span>\
        </div>\
        <div id="stock-value">\
            <div id="stock-value-status">\
                <img src="images/clearArrow.svg">\
                <div id="stock-value-status-currency">USD</div>\
            </div>\
            <div id="stock-value-text">0.<sup>00</sup></div>\
        </div>\
        <div id="investors-btn">\
            <div id="investors-btn-label">STOCK INFORMATION</div>\
            <div id="investors-btn-arrow"><span class="fa fa-angle-double-right"></span></div>\
        </div>\
    </div>'
    $(parentView).append(htmlTemplate);
    that.init();

}
