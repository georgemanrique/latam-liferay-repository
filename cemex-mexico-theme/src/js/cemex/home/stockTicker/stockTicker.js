/*!
 * stockTicker.js
 * Created by Santiago Prada
 * Copyright © 2016 IBM. All rights reserved.
 */

var TickerExchangeModel = function() {
    this.stockValue = 0;
    this.change = 0;
}


var StockTicker = function(parentView, applicationProperties) {

    var that = this;

    this.applicationProperties = applicationProperties

    // //Load component CSS
    var cssId = 'stockTicker-css';
    if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.href = BASE_ASSETS_DIR + 'js/components/stockTicker/stockTicker.css';

        //Insert CSS before Main Css
        var mainCSS = document.getElementById(that.applicationProperties.mainCSSId);
        if (mainCSS) {
            head.insertBefore(link, mainCSS);
        } else {
            head.append(link);
        }
    }

    this.init = function() {

        var activeExchange = "NYSWdata";

        //Properties
        this.NYSE = $("#NYSE", parentView);
        this.BMVM = $("#BMV", parentView);
        this.container = $("#stock-ticker-component");

        //Exchange Models
        //NYSE
        this.NYSWdata = new TickerExchangeModel();
        //MEX
        this.MEXdata = new TickerExchangeModel();

        this.updateUI = function(exchangeName, exchangeModel, view) {

            $(".ticker-stock-label", view).html(exchangeName);
            $(".stock-value-text", view).html(exchangeModel["stockValue"]);

            //Update Arrow based on value change
            if (Number(exchangeModel["change"]) < 0) {
                $("img", view).attr("src", "images/stockDownArrowWhite.svg");
            } else {
                $("img", view).attr("src", "images/stockUpArrowWhite.svg");
            };
        };


        //Load Stock Data

        // IMPORTANT!!!! -> DATA API IS FOR DEVELOPMENT PORPUSES ONLY, GOOGE API HAVE BEEN DEPRECATED
        this.loadNYSEdata = function() {
            $.ajax({
                url: "https://finance.google.com/finance/info?client=ig&q=NSE:,CX",
                dataType: "jsonp",
                success: function(response) {
                    that.NYSWdata["stockValue"] = response[0]["l"];
                    that.NYSWdata["change"] = response[0]["c"];
                    that.updateUI("NYSE", that.NYSWdata, that.NYSE);
                }
            })
        };

        this.loadMEXdata = function() {
            //Data not available for development, fake data in place until integration with web services
            that.MEXdata["stockValue"] = "16.84";
            that.MEXdata["change"] = "0";
            that.updateUI("BMV", that.MEXdata, that.BMVM);
        };
        this.loadNYSEdata();
        this.loadMEXdata();

        // Ticker Animation
        this.flip = 0
        window.setInterval(function(){
              if (that.flip == 0) {
                that.flip = 1;
                that.container.css({
                  "transform" : "translate3d(0, -40px, 0)"
                });
              }else {
                that.flip = 0;
                that.container.css({
                  "transform" : "translate3d(0, 0, 0)"
                });
              };
        }, 4500);
    };


    //Load Template
    // PRODUCTION - Load html template from server
    // var htmlTemplate = $(parentView).load("components/stock/stockTickerTemplate.html", function() {
    //     that.init();
    // });

    // DEVELOPMENT - Local Template
    var htmlTemplate = '<div id="stock-ticker-component">\
        <div id="NYSE" class="stock-market">\
            <div class="ticker-stock-label"></div>\
            <div class="stock-value-text">8.78</div>\
            <img src="images/stockDownArrowWhite.svg">\
        </div>\
        <div id="BMV" class="stock-market">\
            <div class="ticker-stock-label"></div>\
            <div class="stock-value-text">00.00</div>\
            <img src="images/stockDownArrowWhite.svg">\
        </div>\
    </div>';

    $(parentView).append(htmlTemplate);
    that.init();
}
