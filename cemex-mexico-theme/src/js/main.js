(function() {
    AUI().ready(
        'liferay-sign-in-modal',
        function(A) {
            var signIn = A.one('.sign-in > a');

            if (signIn && signIn.getData('redirect') !== 'true') {
                signIn.plug(Liferay.SignInModal);
            }
        }
    );

})();

//Fix hidden button to change template from Web Content Display Portlet Configuartions
    (function($) {
        var onReady = function () {
            $('.template-preview-button').removeClass('hidden');
        };
        
        AUI().ready(onReady);
    })(jQuery);