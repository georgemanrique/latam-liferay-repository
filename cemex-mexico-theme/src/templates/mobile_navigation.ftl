<div id="mobile-nav">
    <div id="mobile-btn"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div id="mobile-nav-container">
        <div id="mobile-search">
            <#include "${full_templates_path}/search.ftl" />
        </div>
        <nav role="navigation">
            <#list nav_items as nav_item>
                <div class="mobile-menu-container">
                    <a class="primary-link ${nav_item.isSelected()?then('mobile-primary-page-active','')}" 
                        href="${nav_item.getURL()}" ${nav_item.getTarget()}>
                        ${nav_item.getName()}
                    </a>
                    <#if nav_item.hasChildren()>
                        <ul class="mobile-sub-menu">
                            <#list nav_item.getChildren() as nav_child>
                                <li class="sub-menu-link">
                                    <a class="secondary-link ${nav_child.isSelected()?then('mobile-secondary-page-active','')}" href="${nav_child.getURL()}" ${nav_child.getTarget()}>
                                        <span class="link-label">${nav_child.getName()}</span>
                                        <span class="link-arrow fa fa-angle-double-right">
                                    </a>
                                </li>
                            </#list>
                        </ul>
                    </#if>
                </div>
            </#list>
        </nav>
    </div>
</div>