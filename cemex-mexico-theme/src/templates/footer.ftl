<footer class="row" style="clear:both">
	<#-- Add a embedded Web Content Display Portlet to show the Country Quick Links -->
	<#-- We insert this as direct child of row becasue it should use 100% of width -->				
	<#if footer_navigation_quick_links != ''>
		<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
		<#assign VOID = freeMarkerPortletPreferences.setValue("groupId", "${group_id}") />
		<#assign VOID = freeMarkerPortletPreferences.setValue("articleId", "${footer_navigation_quick_links}") />

		<@liferay_portlet["runtime"]
			defaultPreferences="${freeMarkerPortletPreferences}"
			portletProviderAction=portletProviderAction.VIEW
			instanceId="footerNavigationQuickLinks"
			portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
		/>

		<#assign VOID = freeMarkerPortletPreferences.reset() />
	</#if>

	<div class="footer-web-content col-md-8">
		<#if footer_navigation_article_id != ''>
			<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
			<#assign VOID = freeMarkerPortletPreferences.setValue("groupId", "${group_id}") />
			<#assign VOID = freeMarkerPortletPreferences.setValue("articleId", "${footer_navigation_article_id}") />

			<@liferay_portlet["runtime"]
				defaultPreferences="${freeMarkerPortletPreferences}"
				portletProviderAction=portletProviderAction.VIEW
				instanceId="footerNavigationContent"
				portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"
			/>

			<#assign VOID = freeMarkerPortletPreferences.reset() />
		</#if>
	</div>

	<div class="col-md-4">		
		<ul class="social-list-inline pull-right" id="social">
			<#if footer_navigation_quick_links != ''>
				<li class="b-quick-access__item">
					<a id="quickLinksFooter" class="b-quick-access__button" href="#">
						<span>CEMEX-México</span>
						<span class="fa fa-chevron-down b-chevron"></span>
					</a>				
				</li>
			</#if>
			<#if show_footer_social_section>
				<#if facebook_url != ''>
					<li>
						<a id="faceBookLink" class="social-link" href="${facebook_url}" title="Facebook"></a>
					</li>
				</#if>

				<#if twitter_url != ''>
					<li>
						<a id="twitterLink" class="social-link" href="${twitter_url}" title="Twitter"></a>
					</li>
				</#if>

				<#if instagram_url != ''>
					<li>
						<a id="instagramLink" class="social-link" href="${instagram_url}" title="Instagram"></a>
					</li>

				</#if>

				<#if youtube_url != ''>
					<li>
						<a class="social-link" href="${youtube_url}" id="youtubeLink" title="Youtube"></a>
					</li>
				</#if>
			</#if>
		</ul>		
	</div>
</footer>