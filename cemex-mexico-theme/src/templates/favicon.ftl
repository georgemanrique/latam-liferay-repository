<link rel="apple-touch-icon" sizes="76x76" href="${images_folder}/apple-touch-icon.png">

<link rel="icon" type="image/png" href="${images_folder}/favicon-32x32.png" sizes="32x32">

<link rel="icon" type="image/png" href="${images_folder}/favicon-16x16.png" sizes="16x16">

<link rel="manifest" href="${images_folder}/manifest.json">

<link rel="mask-icon" href="${images_folder}/safari-pinned-tab.svg" color="#ffffff">

<meta name="theme-color" content="#ffffff">