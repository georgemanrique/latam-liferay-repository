<div id="search-component" class="search">
	<div id="search-icon"><span class="fa fa-search" aria-hidden="true"></span></div>
	<div id="search-form">
		<div id="search-text-field-container">
			<@liferay.search/>
		</div>
	</div>
	<div id="close-search" title="<@liferay.language key="clear-search" />"></div>
</div>