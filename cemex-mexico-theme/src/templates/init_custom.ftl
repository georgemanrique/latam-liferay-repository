<#assign
	facebook_url = theme_display.getThemeSetting("facebook-url")!''
	footer_navigation_article_id = theme_display.getThemeSetting("footer-navigation-article-id")!''
	instagram_url = theme_display.getThemeSetting("instagram-url")!''
	show_footer_social_section = getterUtil.getBoolean(theme_display.getThemeSetting("show-footer-social-section"))
	twitter_url = theme_display.getThemeSetting("twitter-url")!''
	youtube_url = theme_display.getThemeSetting("youtube-url")!''
	color_theme = theme_display.getThemeSetting("color-layout-theme")!''
	footer_navigation_quick_links = theme_display.getThemeSetting("footer_navigation_quick_links")!''
    show_language_selector = getterUtil.getBoolean(theme_display.getThemeSetting("show-language-selector"))
/>

<#assign the_title = "" />

<#if layout.getHTMLTitle(locale)??>
	<#assign the_title = layout.getHTMLTitle(locale) />
</#if>

<#if pageTitle??>
	<#assign the_title = pageTitle />
</#if>

<#if tilesTitle?has_content>
	<#assign the_title = languageUtil.get(locale, tilesTitle) />
</#if>

<#if page_group.isLayoutPrototype()>
	<#assign the_title = page_group.getDescriptiveName(locale) />
</#if>

<#if !tilesTitle?has_content>
	<#assign the_title = htmlUtil.escape(the_title) />
</#if>

<#if the_title ?has_content && !stringUtil.equals(company_name, site_name) && !page_group.isLayoutPrototype()>
	<#assign the_title = the_title + " | " + site_name />
</#if>

<#if !show_control_panel>
	<#assign
	css_class = css_class?replace('controls-visible', '')
	css_class = css_class?replace('has-control-menu', '')
	css_class = css_class?replace('open', '')
	/>
</#if>
