<script>
    BASE_ASSETS_DIR = '${javascript_folder}/../';

    applicationProperties = {
        mainCSSId: "main-css",
        rows: [{
            id: "row-1",
            desktop: function() {
                return window.innerHeight - 100
            },
            tabletLanscape: function() {
                return window.innerHeight - 100
            },
            tabletPortrait: function() {
                return 780
            },
            phone: function() {
                return 700
            },
        }]
    };
</script>

<!-- Load Scripts -->
<#-- 
    To avoid to do a lot of request to the server, those files are unified 
    in a file named all-cemex-scripts.min.js
    If you are a developer and you don't need the minified version use all-cemex-scripts.js
-->
<script src="${javascript_folder}/all-cemex-scripts.min.js"></script>

<script>
    anchors.add('.hero-text, h1, h2, h3, h4, h5').remove(".no-anchor");

    $(document).ready(function() {
        setTimeout(function(){
            var hash = window.location.hash;
            if(hash) {
                $(hash).click();
                $(hash).blur();
            }
        },1000);
    });

    $( ".content a" ).on( "click", function( event ) {
      var href = $(this).attr('href');
      if(href.indexOf("#") == 0) {
          $(href).click();
      }
    });

</script>