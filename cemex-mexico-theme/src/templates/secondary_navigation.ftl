<#if secondary_nav_item??>
    <#if secondary_nav_item.hasChildren()>

        <nav id="secondary-navigation" role="navigation">
            <#list secondary_nav_item.getChildren() as nav_child>
                <a class="secondary-nav-link ${nav_child.isSelected()?then('secondary-page-active','')}" 
                    href="${nav_child.getURL()}" ${nav_child.getTarget()}>${nav_child.getName()}</a>
            </#list>
        </nav>

    </#if>
</#if>
