<!doctype html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>${the_title}</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

	<link id="main-css" rel="apple-touch-icon" href="apple-touch-icon.png">

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

	<#include "${full_templates_path}/enquire.ftl" />

	<@liferay_util["include"] page=top_head_include />

	<#include "${full_templates_path}/owl_carousel.ftl" />

	<#include "${full_templates_path}/magnific_popup.ftl" />

	<#include "${full_templates_path}/favicon.ftl" />

</head>

<body class="${css_class} ${color_theme}">
	<@liferay_util["include"] page=body_top_include />

	<#if show_control_panel>
		<@liferay.control_menu />
	</#if>


	<div id="wrapper" class="cemex-wrapper">

		<header id="header">
			<div class="header-container">

				<div class="company-logo">
					<a href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
						<img alt="${logo_description}" src="${site_logo}" />
						<#if show_site_name>
							<span class="site-name" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
								${site_name}
							</span>
						</#if>
					</a>
				</div>

				<div id="search">
					<#include "${full_templates_path}/search.ftl" />
				</div>
                <#if show_language_selector>
                    <div id="language-selector">
                        <@liferay.languages />
                    </div>
                </#if>
				<nav id="navigation" class="navigation-bar" role="navigation">
					<#include "${full_templates_path}/navigation.ftl" />
				</nav>

			</div>
		</header>

		<div id="mobile-navigation">
			<#include "${full_templates_path}/mobile_navigation.ftl" />
		</div>

		<div id="content-footer-wrapper">
			<main id="content" role="main">
				<#if selectable>
					<@liferay_util["include"] page=content_include />
				<#else>
					${portletDisplay.recycle()}

					${portletDisplay.setTitle(the_title)}

					<@liferay_theme["wrap-portlet"] page="portlet.ftl">
						<@liferay_util["include"] page=content_include />
					</@>
				</#if>
			</main>

			<#include "${full_templates_path}/footer.ftl" />
		</div>

	</div>

</div>

<#include "${full_templates_path}/js_bottom_script_loading.ftl" />

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

</html>