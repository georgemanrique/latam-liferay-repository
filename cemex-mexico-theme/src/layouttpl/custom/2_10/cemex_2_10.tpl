<div class="portlet-layout row">
    <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-12 portlet-column portlet-column-first" id="column-1">
        $processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
    </div>
    <div class="col-xl-10 col-lg-9 col-md-9 col-sm-8 col-xs-12 portlet-column portlet-column-last" id="column-2">
        $processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
    </div>
</div>