<div class="portlet-layout container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 portlet-column portlet-column-first" id="column-1">
            $processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 portlet-column portlet-column-last" id="column-2">
            $processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
        </div>
    </div>
</div>