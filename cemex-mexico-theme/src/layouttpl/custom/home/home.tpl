#set ($jsPath = $themeDisplay.getPathThemeJavaScript())
<script src="$jsPath/cemex/home/main.js" type="text/javascript"></script>

#set ($cssPath = $themeDisplay.getPathThemeCss())
<link rel="stylesheet" type="text/css" href="$cssPath/home.css">


<div class="home_layout cemex-home" id="main-content" role="main">
 
    <!-- content loader -->
    <div id="page-loader"></div>

    <div id="grid-wrapper">

      <div id="left" class="col-xs-8">
        <div id="column-1" class="tile portlet-column">
          $processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
        </div>
      </div>

      <div id="right" class="col-xs-4">
        <div class="row half-height">
          <div class="col-xs-12 full-height">
            <div id="column-2" class="tile portlet-column">
              $processor.processColumn("column-2", "portlet-column-content")
            </div>
          </div>
        </div>

        <div class="row half-height">
          <div class="col-xs-6 full-height">
            <div class="row full-height">
              
              <div class="col-xs-12 half-height">
                <div id="column-5" class="tile portlet-column">
                  $processor.processColumn("column-5", "portlet-column-content")
                </div>
              </div>
              
              <div class="col-xs-12 tile half-height">
                <div id="column-6" class="tile portlet-column">
                  $processor.processColumn("column-6", "portlet-column-content")
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-xs-6 tile full-height">
            <div id="column-7" class="tile portlet-column">
              $processor.processColumn("column-7", "portlet-column-content portlet-column-content")
            </div>
          </div>

        </div>
      </div>

    </div>

    <div class="row b-full-row">
      <div id="bottom" class="col-xs-12 full-height">
        <div id="column-8" class="tile portlet-column">
          $processor.processColumn("column-8", "portlet-column-content portlet-column-content-last")
        </div>
      </div>
    </div>

</div>
